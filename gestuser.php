<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';?>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <?php include 'leftmenu.php'?>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

               <!-- START X-NAVIGATION VERTICAL -->

               <?php include 'topmenu.php'?>
                <!-- END X-NAVIGATION VERTICAL -->

                <!-- PAGE TITLE -->
                <div class="page-title">
                    <h2><span class="fa fa-user"></span> Gérer un Client</h2>
                </div>
                <!-- END PAGE TITLE -->

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">

                            <form class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="row">

                                        <div class="col-md-6">
                                            <span ><strong>Envoie de notifications</strong></span><br>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Cocher</label>
                                                <div class="col-md-9">
                                                    <label class="check"><input type="checkbox" checked="checked"/> Pour envoyer à tous les clients</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">U Email</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Message</label>
                                                <div class="col-md-9 col-xs-12">
                                                    <textarea class="form-control" rows="5"></textarea>
                                                    <span class="help-block">pas plus de 160 caractères</span>
                                                </div>
                                            </div>

                                            <button class="btn btn-primary pull-right">Envoyez notif</button>

                                        </div>
                                        <div class="col-md-6">

                                            <span><strong>Créditer un client</strong></span><br>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">U Email</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Montant</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="number" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-primary pull-right">Créditer</button><br><hr>
                                            <span><strong>Supprimer un client</strong></span><br>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">U Email</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Motif</label>
                                                <div class="col-md-9 col-xs-12">
                                                    <textarea class="form-control" rows="2"></textarea>
                                                    <span class="help-block">pas plus de 160 caractères</span>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary pull-right">Archiver client</button>

                                    </div>

                                </div>
                            </div>
                            </form>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>email</th>
                                                    <th>Nom&prenom</th>
                                                    <th>Numéros</th>
                                                    <th>Statut</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Aspopoameg@gmail.com</td>
                                                    <td>Espoir Ameganvi</td>
                                                    <td>+33753348033</td>
                                                    <td>Activé</td>
                                                </tr>
                                                <tr>
                                                    <td>espopoameg@gmail.com</td>
                                                    <td>Espoir Ameganvi</td>
                                                    <td>+33753348033</td>
                                                    <td>Activé</td>
                                                </tr>
                                                <tr>
                                                    <td>espopoameg@gmail.com</td>
                                                    <td>Espoir Ameganvi</td>
                                                    <td>+33753348033</td>
                                                    <td>Activé</td>
                                                </tr>
                                                <tr>
                                                    <td>espopoameg@gmail.com</td>
                                                    <td>Espoir Ameganvi</td>
                                                    <td>+33753348033</td>
                                                    <td>Activé</td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>

                </div>
                <!-- PAGE CONTENT WRAPPER -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

    <!-- START SCRIPTS -->
    <?php include 'js.php'?>
    <!-- END SCRIPTS -->
    </body>
</html>
