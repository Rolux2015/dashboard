<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';?>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <?php include 'leftmenu.php'?>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

               <!-- START X-NAVIGATION VERTICAL -->

               <?php include 'topmenu.php'?>
                <!-- END X-NAVIGATION VERTICAL -->

                <!-- PAGE TITLE -->
                <div class="page-title">
                    <h2><span class="fa fa-user"></span> Statut des montants aujourd'hui <?php  echo date("d/m/Y") ?></h2>
                </div>
                <!-- END PAGE TITLE -->

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
					<!-- START WIDGETS -->
					<div class="row">
						<div class="col-md-3">
							<!-- START WIDGET MESSAGES -->
							<div class="widget widget-default widget-item-icon">
								<div class="widget-item-left">
									<span class="fa fa-money"></span>
								</div>
								<div class="widget-data">
									<div class="widget-int num-count">Flooz</div>
									<div class="widget-title">50 000Fcfa</div>
									<div class="widget-subtitle">Fait auj</div>
								</div>
								<div class="widget-controls">
									<a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
								</div>
							</div>
							<!-- END WIDGET MESSAGES -->

						</div>
						<div class="col-md-3">

							<!-- START WIDGET MESSAGES -->
							<div class="widget widget-default widget-item-icon">
								<div class="widget-item-left">
									<span class="fa fa-money"></span>
								</div>
								<div class="widget-data">
									<div class="widget-int num-count">Tmoney</div>
									<div class="widget-title">60 000Fcfa</div>
									<div class="widget-subtitle">Fait auj</div>
								</div>
								<div class="widget-controls">
									<a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
								</div>
							</div>
							<!-- END WIDGET MESSAGES -->

						</div>
						<div class="col-md-3">

							<!-- START WIDGET REGISTRED -->
							<div class="widget widget-default widget-item-icon">
								<div class="widget-item-left">
									<span class="fa fa-money"></span>
								</div>
								<div class="widget-data">
									<div class="widget-int num-count">Moov</div>
									<div class="widget-title">10 000Fcfa</div>
									<div class="widget-subtitle">Fait auj</div>
								</div>
								<div class="widget-controls">
									<a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
								</div>
							</div>
							<!-- END WIDGET REGISTRED -->

						</div>
						<div class="col-md-3">

							<!-- START WIDGET CLOCK -->
							<div class="widget widget-default widget-item-icon">
								<div class="widget-item-left">
									<span class="fa fa-money"></span>
								</div>
								<div class="widget-data">
									<div class="widget-int num-count">Togocel</div>
									<div class="widget-title">4 000Fcfa</div>
									<div class="widget-subtitle">Fait auj</div>
								</div>
								<div class="widget-controls">
									<a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
								</div>
							</div>
							<!-- END WIDGET CLOCK -->

						</div>
					</div>
					<!-- END WIDGETS -->
					<div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Flooz</th>
                                                    <th>Tmoney</th>
                                                    <th>Moov</th>
													<th>Togocel</th>
													<th><strong>Total</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?php echo date("d/m/Y")?></td>
                                                    <td>10000</td>
                                                    <td>10000</td>
                                                    <td>10000</td>
													<th>10000</th>
													<th style="color:red">40000</th>
                                                </tr>
												<tr>
                                                    <td><?php echo date("d/m/Y")?></td>
                                                    <td>10000</td>
                                                    <td>10000</td>
                                                    <td>10000</td>
													<th>10000</th>
													<th style="color:red">40000</th>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>

                </div>
                <!-- PAGE CONTENT WRAPPER -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

    <!-- START SCRIPTS -->
	<?php include 'js.php'?>
    <!-- END SCRIPTS -->
    </body>
</html>
