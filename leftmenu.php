<div class="page-sidebar">
	<!-- START X-NAVIGATION -->
	<ul class="x-navigation">
		<li class="xn-profile">
			<a href="#" class="profile-mini">
				<img src="images/users/avatar.jpg" alt="John Doe"/>
			</a>
			<div class="profile">
				<div class="profile-image">
					<img src="images/users/user.jpg" alt="John Doe"/>
				</div>
				<div class="profile-data">
					<div class="profile-data-name">Admin</div>
					<div class="profile-data-title">Admin</div>
				</div>
				<div class="profile-controls">
					<a href="" class="profile-control-left"><span class="fa fa-info"></span></a>
					<a href="" class="profile-control-right"><span class="fa fa-envelope"></span></a>
				</div>
			</div>
		</li>
		<li class="xn-title"><a href="gestuser.php"><span class="fa fa-dashboard"></span> <span class="xn-text">Gestion des utilisateurs</span></a></li>
		<li class="xn-title"><a href="blacklist.php"><span class="fa fa-dashboard"></span> <span class="xn-text">Gestion du blacklist</span></a></li>
		<li class="xn-title"><a href="refunduser.php"><span class="fa fa-dashboard"></span> <span class="xn-text">Gestion de Remboursement </span></a></li>
		<li class="xn-title"><a href="statutsim.php"><span class="fa fa-dashboard"></span> <span class="xn-text">Statut des montants</span></a></li>

	</ul>
	<!-- END X-NAVIGATION -->
</div>
