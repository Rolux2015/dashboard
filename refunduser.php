<!DOCTYPE html>
<html lang="en">
<?php include 'head.php';?>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <?php include 'leftmenu.php'?>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

               <!-- START X-NAVIGATION VERTICAL -->
			   	<?php include 'topmenu.php'; ?>
                <!-- END X-NAVIGATION VERTICAL -->

                <!-- PAGE TITLE -->
                <div class="page-title">
                    <h2><span class="fa fa-user"></span> Rembourser un Client</h2>
                </div>
                <!-- END PAGE TITLE -->

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>email</th>
                                                    <th>Nom&prenom</th>
                                                    <th>Numéros</th>
                                                    <th>Statut</th>
													<th>Factures</th>
													<th>refund</th>
													<th>Etat Remboursement</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Aspopoameg@gmail.com</td>
                                                    <td>Espoir Ameganvi</td>
                                                    <td>+33753348033</td>
                                                    <td>Activé</td>
													<th>Fact 001</th>
													<td><button class="btn btn-warning btn-condensed"><i class="fa fa-rotate-left"></i></button></td>
													<th>R</th>
                                                </tr>
												<tr>
                                                    <td>Aspopoameg@gmail.com</td>
                                                    <td>Roland AMEGADJE</td>
                                                    <td>+33753348033</td>
                                                    <td>Activé</td>
													<th>Fact002</th>
													<td><button class="btn btn-warning btn-condensed"><i class="fa fa-rotate-left"></i></td>
													<th>N/R</th>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>

                </div>
                <!-- PAGE CONTENT WRAPPER -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
    <!-- START SCRIPTS -->
    <?php include 'js.php'?>
    <!-- END SCRIPTS -->
    </body>
</html>
